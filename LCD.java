public class LCD {
    private String Status;
    private int Volume;
    private int Brightness;
    private String Cable;

    public void turnOff (){
        this.Status = "Off";
    }

    public void turnOn(){
        this.Status = "On";
    }
    
    public void Freeze(){
        this.Status = "Freeze";
    }

    public int volumeUp(int a){
        if (this.Status == "On" || this.Status == "Freeze"){
            this.Volume++;}
        return this.Volume;
        
    }
    
    public int volumeDown(){
        if (this.Status == "On"|| this.Status == "Freeze"){
            this.Volume--;}
        return this.Volume;
    }

    public int setVolume(int Volume){
        if (this.Status == "On" || this.Status == "Freeze"){
            this.Volume = Volume;}
        return this.Volume;
    }

    public int brightnessUp(){
        if (this.Status == "On" || this.Status == "Freeze"){
            this.Brightness++;
                }return this.Brightness;
    }

    public int brightnessDown(){
        if (this.Status == "On" || this.Status == "Freeze"){
        this.Brightness--;
            }return this.Brightness;
       
    }

    public int setBrightness(int brigtness){
        if (this.Status == "On"  || this.Status == "Freeze" ){
           this.Brightness = brigtness; 
        }
        return this.Brightness;
    }
    
    public void cableUp(){
        if (this.Status == "On" || this.Status == "Freeze" ){
        if (this.Cable == "VGA"){
            this.Cable = "VGI";
        }
        else if (this.Cable == "VGI"){
            this.Cable = "HDMI";
        }
        else if (this.Cable == "HDMI"){
            this.Cable = "HDMI";
        }}
    }
  

    public void cableDown(){
        if (this.Status == "On" || this.Status == "Freeze" ){
        if (this.Cable == "HDMI"){
            this.Cable = "VGI";
        }
        else if (this.Cable == "VGI"){
            this.Cable = "VGA";
        }
        else if (this.Cable == "VGA"){
            this.Cable = "VGA";
        }}
     }
    

    public void setCable(String cable){
        if (this.Status == "On" || this.Status == "Freeze" ){
       this.Cable = cable;}
    }


    public void display(){
        System.out.println("==========LCD==========");
        System.out.println("Status\t\t: "+this.Status);
        System.out.println("Volume\t\t: "+this.Volume);
        System.out.println("Brightness\t: "+this.Brightness);
        System.out.println("Cable\t\t: "+this.Cable);
    }






    
}
