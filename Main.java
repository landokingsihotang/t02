public class Main{
    public static void main(String[] args) {
        
        LCD lcd1 = new LCD ();
        lcd1.turnOff();
        lcd1.turnOn();
        lcd1.setVolume(40);
        lcd1.volumeDown();
        lcd1.setBrightness(20);
        lcd1.brightnessUp();
        lcd1.setCable("HDMI");
        lcd1.cableDown();
        lcd1.display();
        
    }
}